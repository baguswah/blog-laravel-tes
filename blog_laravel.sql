-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2023 at 06:54 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog_laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `description`, `user_id`, `created_at`, `updated_at`) VALUES
(3, '#544: Casey Newton and Kevin Roose', 'CN: “People actually like to be a little bit confused. They like listening to things where people are talking about things they don’t quite understand, which was very counterintuitive to me. I think a lot of editor-types would scoff at, but I’ve come around.”\r\nKR: “We can revisit subjects and we do. We can change our minds. Print pieces feel so permanent, they feel so definitive. Podcasts, we can just sort of say, ‘I don\'t know what to make of this, ask me again in a month.’”\r\n\r\nCN: “People actually like to be a little bit confused. They like listening to things where people are talking about things they don’t quite understand, which was very counterintuitive to me. I think a lot of editor-types would scoff at, but I’ve come around.”\r\nKR: “We can revisit subjects and we do. We can change our minds. Print pieces feel so permanent, they feel so definitive. Podcasts, we can just sort of say, ‘I don\'t know what to make of this, ask me again in a month.’”\r\n\r\n\r\nCN: “People actually like to be a little bit confused. They like listening to things where people are talking about things they don’t quite understand, which was very counterintuitive to me. I think a lot of editor-types would scoff at, but I’ve come around.”\r\nKR: “We can revisit subjects and we do. We can change our minds. Print pieces feel so permanent, they feel so definitive. Podcasts, we can just sort of say, ‘I don\'t know what to make of this, ask me again in a month.’”', 2, '2023-08-15 12:07:58', '2023-08-16 09:02:51'),
(4, '#539: Mitchell Prothero', '“I’m really interested in transnational networks—crime, intelligence. I’m fascinated by the gray. Like, when is something legal and when is something illegal? One thing with this Gateway project [was that] nobody could ever tell me that moment where money goes from absolutely being illegal to being legal.”', 2, '2023-08-15 12:10:39', '2023-08-15 21:49:24'),
(5, '#536: Lisa Belkin', '“I didn’t experience it as luck. It—and this is going to be a little woo woo—but it really felt like these people had been sitting there for 100 years saying, Well, it took you long enough, because everything just fit together. I didn’t have to manipulate anything.”', 2, '2023-08-15 12:17:45', '2023-08-15 21:49:48'),
(6, '#521: Jonah Weiner', '“It\'s a version of myself. It\'s a hyperbolic version of myself. And I think it keeps it fun for me. It doesn\'t feel like a job. Ideally, it keeps it fun for readers. And I think that there actually is this function where X out of 10 people coming to it, their eyes are going to cross and they\'re going say, I\'m out. No thanks. And that\'s fine, because the Y out of 10 who stick around feel that much more in on something and it just makes it feel like a funky, special place.”', 1, '2023-08-15 12:18:29', '2023-08-15 21:50:48'),
(8, '#520: Delia Cai', '“This was in like, 2011, where I think actual journalists were still trying to figure out ‘Is it gross to be a brand?’ And at least in school, they were all about it. They’re like, ‘You need a brand, you need to think about what your niche is going to be, you need to think about engaging your audience.’ We had to make websites, we had to blog, and of course, all of us being college students, we started using our blogs to write about each other. We used Twitter to talk shit about each other in a very thinly veiled way. So really, it was the best training for being online.”', 1, '2023-08-15 12:25:23', '2023-08-15 21:51:27'),
(10, '#519: Peggy Orenstein', '“The challenge is… to not want to say, I need to know what the book is about. I need to have my chapters. I need to know what exactly I\'m looking for. Because it\'s really scary to just go out and report and have trust that there\'s going to be interesting things and that if you just keep going, you\'re going to find them. So to not foreclose possibility and options and ideas is the biggest reporting challenge for those sorts of books for me.”', 2, '2023-08-15 16:46:36', '2023-08-15 21:56:47'),
(11, '#518: Jonathan Goldstein', '“I wasn’t taking myself very seriously, initially. I liked working with my friends and family because I think I was a little more comfortable with them. Then in the second season people were writing in with real problems, and they were looking at me as a kind of expert. It was terrifying to meet with these people and see the look of hopefulness in their eyes. ... I realized I need to step it up and even if I didn’t feel like an expert—an expert in an invented field that doesn’t really exist—that I’d really have to take that on with seriousness.”', 2, '2023-08-15 18:35:34', '2023-08-15 21:57:23'),
(12, 'asd', 'asd', 1, '2023-08-16 09:33:14', '2023-08-16 09:33:14');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `blogs_id` bigint(20) UNSIGNED NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `blogs_id`, `message`, `created_at`, `updated_at`) VALUES
(1, 1, 10, 'adasdasdas', '2023-08-15 17:00:00', '2023-08-15 17:00:00'),
(3, 2, 3, 'test', '2023-08-16 07:45:09', '2023-08-16 07:45:09'),
(4, 1, 4, 'dasd', '2023-08-16 07:56:52', '2023-08-16 07:56:52'),
(5, 1, 11, 'jangan error', '2023-08-16 08:08:50', '2023-08-16 09:01:35');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(15, '2014_10_12_100000_create_password_resets_table', 1),
(16, '2019_08_19_000000_create_failed_jobs_table', 1),
(17, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(18, '2023_08_15_135312_create_users_table', 1),
(19, '2023_08_15_140512_create_blogs_table', 1),
(20, '2023_08_15_141427_create_comments_table', 1),
(21, '2023_08_15_150005_alter_table_comments', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Bagus', 'bagus@mail.com', 'e00cf25ad42683b3df678c61f42c6bda', NULL, NULL),
(2, 'Charli', 'charli@mail.com', '927f44d99c5dccea532216d1b52bab87', '2023-08-15 23:45:54', '2023-08-15 23:45:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blogs_user_id_foreign` (`user_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_blogs_id_foreign` (`blogs_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_blogs_id_foreign` FOREIGN KEY (`blogs_id`) REFERENCES `blogs` (`id`),
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
