<!--===================== NAVBAR TOP =====================-->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">

  <!-- hAMBURGER SIDEBAR -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>

  </ul>
  <!-- END -->



  <!-- NAVBAR RIGHT -->
  <ul class="navbar-nav ml-auto">

    <!-- PROFILE -->
    <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <span class="text-bold">{{ session('Name') }}</span>
          <i class="fas fa-user" style="width:100%; margin-left:5px;"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
          <a href="{{ url('/editprofil') }}" class="dropdown-item">
            Pengaturan
          </a>
          <div class="dropdown-divider"></div>
          <a href="{{ url('/change-password') }}" class="dropdown-item">
            Ubah Password
          </a>
          <div class="dropdown-divider"></div>
          <a href="{{ url('/logout') }}" class="dropdown-item">
            Keluar
          </a>
          <div class="dropdown-divider"></div>
        </div>
      </li>
    <!--<li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button"><i
            class="fas fa-user"></i></a>
      </li>-->
    

  </ul>
  <!-- END NAVBAR RIGHT -->

</nav>
<!--===================== END =====================-->