@extends('layout/main')
@section('menu1', 'menu-open')
@section('Sub-menu-1', 'active-sidebar')
<!--===================== TITLE =====================-->
@section('title')
Blog Laravel
@endsection
<!--===================== END =====================-->

<!--===================== CONTENT =====================-->
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-bold" style="font-size: 26px; ">Create New Blog</h1>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card card-primary">
              </div>
              <form action="{{ route('blog.store') }}" method="post">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label>Title</label>
                    <input type="text" name="blogTitle" class="form-control" id="inputBlog" placeholder="Title" required>
                    <label class="my-2">Content</label>
                    <textarea name="blogDesc" placeholder="Input Content" id="editor" cols="30" rows="5" class="form-control" required></textarea>
                    <label class="my-2">Authors</label>
                    <select name="userId" class="form-control" required>
                        <option value="">Pilih Authors</option>
                        @foreach ($dataUser as $selectauthor)
                        <option value="{{ $selectauthor->id }}">{{ $selectauthor->name }}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                  <a href="{{ url('/blog') }}" type="submit" class="btn btn-outline-danger btn-submit">Cancel</a>
                </div>
              </form>
            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
   ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .then( editor => {
            // Menyinkronkan isi editor dengan textarea saat form di-submit
            editor.model.document.on('change', () => {
                document.querySelector('#editor').value = editor.getData();
            });
        } )
        .catch( error => {
            console.error( error );
        } );
</script>

@endsection

<!--===================== END =====================-->


{{-- <script>
$(document).ready(function (){
    $('.btn-submit').on('click', function (e){
        e.preventDefault();
        Swal.fire({
            title: 'Konfirmasi',
            text:'Apakah anda yakin ingin menambah Brand baru?',
            showCancelButton:true,
            closeOnConfirm:true,
            confirmButtonText:"Ya",
            cancelButtonText : "Tidak",
        }).then(willSend) => {
            if(willSend.isConfirmed) {
                $('form').submit();
            }
        }
    })
})
</script> --}}