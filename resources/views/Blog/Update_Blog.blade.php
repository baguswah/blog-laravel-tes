@extends('layout/main')
@section('menu1', 'menu-open')
@section('Sub-menu-1', 'active-sidebar')
<!--===================== TITLE =====================-->
@section('title')
Blog Laravel
@endsection
<!--===================== END =====================-->

<!--===================== CONTENT =====================-->
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-bold" style="font-size: 26px; ">Edit Blog</h1>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card card-primary">
              </div>
              <form action="{{ url('blog/update') }}" method="post">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <input type="hidden" name="id" class="form-control" id="id" placeholder="Title" value="{{ $blog->id }}" required>
                    <label>Title</label>
                    <input type="text" name="blogTitle" class="form-control" id="title" placeholder="Title" value="{{ $blog->title }}" required>
                    <label class="my-2">Content</label>
                    <textarea name="blogDesc" placeholder="Input Content" id="description" cols="30" rows="5" class="form-control" required>{{ $blog->description }}</textarea>
                    <label class="my-2">Authors</label>
                    <select name="userId" class="form-control" required>
                        @foreach ($dataUser as $selectauthor)
                        <option value="{{ $selectauthor->id }}"@if ($selectauthor->id == $blog->user_id) selected @endif>{{ $selectauthor->name }}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                    <a href="{{ url('/blog') }}" type="submit" class="btn btn-outline-danger btn-submit">Cancel</a>
                </div>
                </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- <script>
   $(document).ready(function (){
    $('.btn-submit').on('click', function (e){
        e.preventDefault();
        Swal.fire({
            title: 'Konfirmasi',
            text:'Apakah anda yakin ingin menambah Brand baru?',
            showCancelButton:true,
            closeOnConfirm:true,
            confirmButtonText:"Ya",
            cancelButtonText : "Tidak",
        }).then(willSend) => {
            if(willSend.isConfirmed) {
                $('form').submit();
            }
        }
    })
})
</script> --}}

@endsection
<!--===================== END =====================-->
