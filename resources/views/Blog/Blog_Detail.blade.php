<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Blog Home</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet" />

    </head>
    <body>
        <!-- Responsive navbar-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="#!">Blog Laravel</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li class="nav-item"><a class="nav-link" href="{{ url('/') }}">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('/login') }}">Login</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Page header with logo and tagline-->
        <header class="py-3 bg-light border-bottom mb-4">
            <div class="container">
                <a href="{{ url('/') }}"><div class="btn btn-dark">Back</div></a>
                <div class="text-center my-5">
                    <h1 class="fw-thin">{{ $blog->title }}</h1>
                </div>
                <div class=" text-center mt-5">
                <span>{{ $blog->users->name }},</span>
                <span>{{$blog->created_at}}.</span>
                </div>
            </div>
        </header>
        <!-- Page content-->
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="mt-5 mb-5">
                        <p class="text-black text-md-start">{{ $blog->description }}</p>
                    </div>
                </div>
            </div>
            {{-- CommentArea --}}
            {{-- <div class="comment-area mt-4">
                <div class="card card-body">
                    <h6 class="card-title" >Leave a comment</h6>
                        <form action="{{ url('comments') }}" method="POST">
                            <input type="hidden" name="post_slug" value="{{ $post->slug }}">
                            <textarea name="comment_body" class="form-control" rows="3" required></textarea>
                                <button type="submit" class="btn btn-primary mt-3">Submit</button>
                        </form>
                </div>
            </div>
            <div class="card card-body shadow-sm mt-3">
                <div class="detail-area">
                    <h6 class="user-name mb-1">
                        User One
                    <small class="ms-3 text-primary" >Commented on: 3-8-2022</small>
                    </h6>
                <p class="user-comment mb-1">
                    data into database using Laravel Insert data into
                    database using Laravel Insert data into database using LaravelInsert data into
                </p>
            </div>
            <div>
                <a href="" class="btn btn-primary btn-sm me-2">Edit</a>
                <a href="" class="btn btn-danger btn-sm me-2">Delete</a>
            </div>
            </div> --}}
        </div>


        <!-- Footer-->
        <footer class="py-5 bg-dark">
            <div class="container"><p class="m-0 text-center text-white">Copyright &copy; Your Website 2023</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="{{ asset('js/scripts.js') }}"></script>
    </body>
</html>
