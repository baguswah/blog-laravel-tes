<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Blogging</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/jquery-ui/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/sweetalert2/sweetalert2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/buttons.bootstrap4.min.css') }}">
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    </head>
    <body>
        <!-- Responsive navbar-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="#!">Blogging</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li class="nav-item"><a class="nav-link" href="{{ url('/') }}">Home</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="Login Feature Under Maintenance" href="{{ url('/login') }}">Login</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Page header with logo and tagline-->
        <header class="py-3 bg-light border-bottom mb-4">
            <div class="container">
                <div class="text-center my-5">
                    <h1 class="fw-bolder">Welcome to Blogging!</h1>
                    <p class="lead mb-0">New Articel New Journey</p>
                </div>
            </div>
        </header>
        <!-- Page content-->
        <table  id="artikel">
        <div class="container">
            <div class="row col-9">
                @foreach ($dataBlog as $blog)
                <div class="mb-5">
                    <a href="{{url('/detail-blog/'.$blog->id)  }}"><h3 class="judul">&bull; {{ $blog->title }}</h3></a>
                    <p class="text-secondary">{{ Str::limit($blog->description,300)}}</p>
                    <span>{{ $blog->users->name }},</span>
                    <span>{{$blog->created_at}},</span>
                    <span>(0) Comments</span>
                </div>
                @endforeach

            </div>
        </div>
        </table>
        <!-- Footer-->
        <footer class="py-5 bg-dark">
            <div class="container"><p class="m-0 text-center text-white">Copyright &copy; Your Website 2023</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="{{ asset('js/scripts.js') }}"></script>
        <script src="{{ asset('plugins/inputmask/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('ckeditor/ckeditor.js')}}"></script>
    <script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('plugins/sparklines/sparkline.js') }}"></script>
    <script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    
    <script>
        $(function () {
	    $('[data-toggle="tooltip"]').tooltip();
		});
 
        // $(document).ready(function () { 
        //     var table = $('#artikel').DataTable({ 
        //         dom: 'Brtip', 
        //         lengthMenu: [ 
        //         [ 5, 10, 20, 30, 50, -1 ], 
        //         [ '5 rows','10 rows', '20 rows','30 rows', '50 rows', 'Show all' ] 
        //     ], 
        //     buttons: [ 
        //         'pageLength' 
        //     ] 
        //     }); 
     
        //     $('#searchButton').on('click', function() { 
        //         var searchText = $('#searchInput').val(); 
        //         table.search(searchText).draw(); 
        //     }); 
     
        //     $('#resetButton').on('click', function() { 
        //         $('#searchInput').val(''); 
        //         table.search('').draw(); 
        //     }); 
        // }); 
    </script>
    
    </body>
</html>
