@extends('layout/main')
@section('menu1', 'menu-open')
@section('Sub-menu-2', 'active-sidebar')
<!--===================== TITLE =====================-->
@section('title')
Blog Laravel
@endsection
<!--===================== END =====================-->

<!--===================== CONTENT =====================-->
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-bold" style="font-size: 26px;">Comment Dashboard</h1>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table id="tableBrand" class="table table-striped table-bordered" style="width:100%">
                             <div class="mb-3 d-md-flex justify-content-md-end">
                                <a href="{{ url('/create-comment-view') }}" class="btn btn-success "><i class="fa fa-plus"></i> Comment</a>
                            </div>
                            <div class="row mb-4 gap-3">
                                <div class="col form-outline mx-3">
                                    <input type="text" class="form-control" id="searchInput" placeholder="Search"></div>
                                    <input class="btn btn-primary col-2 mx-2" type="button" id="searchButton" value="Search" />
                                    <input class="btn btn-danger col-2 mx-2" type="button" id="resetButton" value="Reset" />
                            </div>
                            @if (session('success'))
                            <div class="my-3">
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            </div>
                            @endif
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>User</th>
                                    <th>Message</th>
                                    <th>Date</th>
                                    <th class="d-flex justify-content-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($dataComment as $comment)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $comment->users->name }}</td>
                                        <td>{{ $comment->message }}</td>
                                        <td>{{ $comment->created_at }}</td>
                                        <td>
                                            <div class="d-flex justify-content-center">
                                                <a href="{{url('/edit-comment/'.$comment->id)  }}" class="btn btn-outline-success btn-sm"><i class="fa fa-pen"></i> Edit</a>
                                                <div style="margin: 0 10px;"></div>
                                                <a href="{{ url('/delete-comment/'.$comment->id) }}" type="button" class="btn btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>      
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function () { 
        var table = $('#tableBrand').DataTable({ 
            dom: 'Brtip', 
            lengthMenu: [ 
            [ 5, 10, 20, 30, 50, -1 ], 
            [ '5 rows','10 rows', '20 rows','30 rows', '50 rows', 'Show all' ] 
        ], 
        buttons: [ 
            'pageLength' 
        ] 
        }); 
 
        $('#searchButton').on('click', function() { 
            var searchText = $('#searchInput').val(); 
            table.search(searchText).draw(); 
        }); 
 
        $('#resetButton').on('click', function() { 
            $('#searchInput').val(''); 
            table.search('').draw(); 
        }); 
    }); 
</script>   
@endsection
<!--===================== END =====================-->
