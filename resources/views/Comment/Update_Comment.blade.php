@extends('layout/main')
@section('menu1', 'menu-open')
@section('Sub-menu-2', 'active-sidebar')
<!--===================== TITLE =====================-->
@section('title')
Blog Laravel
@endsection
<!--===================== END =====================-->

<!--===================== CONTENT =====================-->
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-bold" style="font-size: 26px; ">Comment</h1>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card card-primary">
              </div>
              <form action="{{ url('comment/update') }}" method="post">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <input type="hidden" name="id" class="form-control" id="id" placeholder="id" value="{{ $comment->id }}" required>
                    <label class="my-2">Penulis</label>
                    <select name="userId" class="form-control" required>
                        @foreach ($dataUser as $selectauthor)
                         <option value="{{ $selectauthor->id }}" @if ($selectauthor->id == $comment->user_id) selected @endif>{{ $selectauthor->name }}</option>
                        @endforeach
                    </select>
                    <label class="my-2">Blog ID</label>
                    <select name="blogId" class="form-control" required>
                    <option >{{ $comment->blogs_id }}</option>
                        @foreach ($dataBlogs as $selectblog)
                        <option value="{{ $selectblog->id }}">{{ $selectblog->id }}</option>
                        @endforeach
                    </select>
                    <label>Message</label>
                    <textarea type="text" name="message" class="form-control" id="message" placeholder="Message" required>{{ $comment->message }}</textarea>
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                  <a href="{{ url('/comment') }}" type="submit" class="btn btn-outline-danger btn-submit">Cancel</a>
                </div>
              </form>
            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
<script>
   
<!--===================== END =====================-->


$(document).ready(function (){
    $('.btn-submit').on('click', function (e){
        e.preventDefault();
        Swal.fire({
            title: 'Konfirmasi',
            text:'Apakah anda yakin ingin menambah Brand baru?',
            showCancelButton:true,
            closeOnConfirm:true,
            confirmButtonText:"Ya",
            cancelButtonText : "Tidak",
        }).then(willSend) => {
            if(willSend.isConfirmed) {
                $('form').submit();
            }
        }
    })
})
</script>