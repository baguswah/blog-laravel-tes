@extends('layout/main')
@section('menu1', 'menu-open')
@section('Sub-menu-2', 'active-sidebar')
<!--===================== TITLE =====================-->
@section('title')
Blog Laravel
@endsection
<!--===================== END =====================-->

<!--===================== CONTENT =====================-->
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-bold" style="font-size: 26px; ">Comment</h1>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card card-primary">
              </div>
              <form action="{{ route('comment.store') }}" method="post">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label class="my-2">Penulis</label>
                    <select name="userId" class="form-control" required>
                        <option value="">Pilih Authors</option>
                        @foreach ($dataUser as $selectauthor)
                        <option value="{{ $selectauthor->id }}">{{ $selectauthor->name }}</option>
                        @endforeach
                    </select>
                    <label class="my-2">Blog ID</label>
                    <select name="blogId" class="form-control" required>
                        <option value="">Pilih Blog ID</option>
                        @foreach ($dataBlog as $selectblog)
                        <option value="{{ $selectblog->id }}">{{ $selectblog->id }}</option>
                        @endforeach
                    </select>
                    <label>Message</label>
                    <textarea type="text" name="message" class="form-control" id="message" placeholder="Message" required></textarea>
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                  <a href="{{ url('/comment') }}" type="submit" class="btn btn-outline-danger btn-submit">Cancel</a>
                </div>
              </form>
            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection