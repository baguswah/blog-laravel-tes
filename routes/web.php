<?php
use App\Http\Controllers\CommentController;
use App\Http\Controllers\BlogController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'BlogController@index');
Route::get('/login', 'BlogController@home');

Route::get('/blog', 'BlogController@home');
Route::get('/create-blog-view', 'BlogController@createview');
Route::post('/blog', 'BlogController@store')->name('blog.store');
Route::get('/edit-blog/{id}', 'BlogController@show');
Route::post('/blog/update','BlogController@update');
Route::get('/delete-blog/{id}', 'BlogController@destroy');
Route::get('/detail-blog/{id}','BlogController@detail');


Route::get('/create-comment-view', 'CommentController@createview');
Route::get('/comment','CommentController@index');
Route::get('/edit-comment/{id}', 'CommentController@show');
Route::post('/comment/update','CommentController@update');
Route::post('/comment', 'CommentController@store')->name('comment.store');
Route::get('/delete-comment/{id}', 'CommentController@destroy');