<?php

namespace App\Http\Controllers;
use App\Models\Comment;
use App\Models\Blog;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataComment = Comment::all();
        return view('Comment/Comment_List', compact('dataComment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createview()
    {
         $dataBlog= Blog::all();
        $dataUser= User::all();
        return view('Comment/Create_Comment', compact('dataBlog', 'dataUser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $users = User::find($request->userId);
         Comment::create([
            'blogs_id' => $request->blogId,
            'user_id' => $request->userId,
            'message' => $request->message,
        ]);

        return redirect('/comment')->with('success', 'Berhasil membuat blog baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function show($id){
    $comment = Comment::find($id); // Ambil data komentar berdasarkan ID yang diberikan
    $dataUser = User::all();
    $dataBlogs = Blog::all();
    return view('Comment/Update_Comment', compact('comment', 'dataUser', 'dataBlogs'));
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
   {
        $updatecomment = Comment::find($request->id);
        $updatecomment->blogs_id = $request->blogId;
        // dd($request->blogId);
        $updatecomment->user_id = $request->userId;
        $updatecomment->message = $request->message;
        if ($updatecomment->save()) {
            return redirect('/comment');
        } else {
            return redirect()->back()->withInput();
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function destroy($id)
    {
        $comment = Comment::destroy($id);
        return redirect('/comment');
    }
}
