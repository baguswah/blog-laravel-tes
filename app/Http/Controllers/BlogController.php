<?php

namespace App\Http\Controllers;
use App\Models\Blog;
use App\Models\User;
use App\Models\Comment;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataBlog = Blog::all();
        return view('Blog/Blog', compact('dataBlog'));
    }
    
    public function home()
    {
        $dataBlog = Blog::all();
        return view('Blog/Blog_List', compact('dataBlog'));
    }

     public function detail(Blog $blog, $id, User $users, Comment $comment,){
        $blog = Blog::find($id);
        $dataUser = User::all();
        $comments = Comment::where('blogs_id', $id)->get(); // Ambil komentar berdasarkan blog_id
        return view('Blog/Blog_Detail', ['blog' => $blog, 'comments' => $comments], compact('dataUser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createview()
    {
       
        $dataUser= User::all();
        return view('Blog/Create_blog', compact('dataUser'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $users = User::find($request->userId);
         Blog::create([
            'title' => $request->blogTitle,
            'description' => $request->blogDesc,
            'user_id' => $users->id,
            'name' => $users->name
        ]);

        return redirect('/blog')->with('success', 'Berhasil membuat blog baru');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show (Blog $blog, $id, User $users){
        $blog = Blog::find($id);
        $dataUser = User::all();
        return view('Blog/Update_Blog', ['blog' => $blog,], compact('dataUser'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     $updateblog = Blog::find($request->id);
    //     $updatebrand->id = $request->id;
    //     $updateblog->title = $request->blogTitle;
    //     $updateblog->description = $request->blogDesc;
    //     $updateblog->name = $request->name;
    //     // dd($updateblog);
    //     if ($updateblog->save()) {
    //         return redirect('/blog');
    //     } else {
    //         return redirect()->back()->withInput();
    //     }
    // }

     public function update(Request $request)
    {
        $updateblog = Blog::find($request->id);
        $updateblog->id = $request->id;
        $updateblog->title = $request->blogTitle;
        $updateblog->description = $request->blogDesc;
        $updateblog->user_id = $request->userId;

        if ($updateblog->save()) {
            return redirect('/blog');
        } else {
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::destroy($id);
        return redirect('/blog');
    }
}
